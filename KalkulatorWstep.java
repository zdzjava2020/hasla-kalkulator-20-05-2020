import java.lang.Math;

public class KalkulatorWstep {
	public static void main(String[] args) {
		
		//tutaj mamy do czynienia z operatorem arytmnetycznym; następuje zsumowanie dwóch wartości literałowych ze sobą
		int suma = 5+6;
		//tutaj obsługa różnicy
		int roznica= 145-40;
		int roznica2= 15-40;
		//mnożenie 
		int iloczyn= 6*5;
		int iloczyn2=6*-5;
		//dzielenie
		int iloraz= 16/2;
		//tutaj nastąpi błąd!
		//int iloraz2= 16/0;
		
		//modulo czyli reszta z dzielenia
		int modulo = 5%2;
		int modulo2=13%25;
		
		//potęgowanie liczb
		int potega= (int)Math.pow(2, 7);
		int potega2= (int)Math.pow(100000, 0);
		
		//pierwiastkowanie
		int pierwiastek = (int)Math.sqrt(9);
		double pierwiastek2 = Math.pow(8,1/3.f);
		
		//INKREMENTACJA (podnoszenie wartości o 1)
		int inc=1;
		inc++; //wartość wynosi 2
		--inc; //wartość wynosi 1
		
		//DEKREMENTACJA (obniżenie wartości o 1)
		int dec=1;
		dec--; //wartość wynosi 0
		++dec; //wartość wynosi 1
		
		double liczba = 4.;
		System.out.println(liczba);
		
		//poniżej Java dokonała niejawnej konwersji z wartości całkowitej (liczbowej) na wartość znakową (STring) i
		//dodała tak przetworzone wartości znaków (kolejne znaki) do wcześniej zapisanych znaków w literalne
		//OCZYWIŚCIE NIEJAWNIE UTWORZYŁA NOWY CIĄG ZNAKOWY
		System.out.println("Suma wartości 5 i 6 = " + suma);
		System.out.println("Różnica wartości 145 i 40 = " + roznica);
		System.out.println("Różnica wartości 15 i 40 = " + roznica2);
		System.out.println("Iloczyn wartości 6 i 5 = " + iloczyn);
		System.out.println("Iloczyn wartości 6 i -5 = " + iloczyn2);
		System.out.println("Iloraz wartości 16 i 2 = " + iloraz);
		//nie ma zmiennej - nie można dzielić przez ZERO
		//System.out.println("Iloraz wartości 16 i 0 = " + iloraz2);
		System.out.println("Modulo wartości 5 i 2 = " + modulo);
		System.out.println("Modulo wartości 13 i 25 = " + modulo2);
		
		System.out.println("Potęga wartości 2 do 7 = " + potega);
		System.out.println("Potęga wartości 100000 do 0 = " + potega2);
		
		System.out.println("Pierwiastek stopnia 2 z 9 = " + pierwiastek);
		System.out.println(pierwiastek2);
		
		
		System.out.println("=================================================================");
		
		System.out.println("Postinkrementacja wartości " + inc + ": " + inc++); //w tej linii wartość inc podnosi się o jeden ale PO JEJ WYKONANIU
		System.out.println("Preinkrementacja wartości " + inc + ": " + ++inc); //w tej linii wartość inc podnosi się o jeden ale PRZED JEJ WYKONANIEM (inkrementacji)
	
		System.out.println("Postdekrementacja wartości " + dec + ": " + dec--);
		System.out.println("Predekrementacja wartości " + dec + ": " + --dec);
	}
}
