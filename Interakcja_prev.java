import java.util.Scanner;

public class Interakcja_prev {

	public static void main(String[] args) {
		int sumaSily = 100;
		int czesciHasla = 5;
		int silaHasla = 8;
		int sila = 0;
		
		
		System.out.println("Witaj w programie liczącym siłę hasła.");
		System.out.print("Podaj hasło, którego siłę chcesz mierzyć: ");
		Scanner wejscie = new Scanner(System.in);
		String haslo = wejscie.next();
		
		//Tutaj by pozyskać wartość prawy lub fałszu można wykorzystywać warunki znane z matematyki
		//Następujące możliwości badania warunków oraz przykłady, kiedy dadzą wartość true:
		
		// ==  - wartość A i B są sobie równe (np. A == B, czyli 5 == 5)
		// > - wartość A jest większa od wartości B (np. A > B, czyli 5 > 3)
		// < - wartość A jest mniejsza od wartości B (np. A < B, czyli 5 < 10)
		// >= - wartość A jest większa bądz równa wartości B (np. A >= B, czyli 5 >= 5 lub 5 >= 3)
		// <= - wartość A jest mniejsza bądz równa wartości B (np. A <= B, czyli 5 <= 5 lub 5 <= 10)
		// != - wartość A jest różna od wartości B (np. A != B, czyli 5 != 6)
		// ! - negowanie wartości A (np. !A, czyli !false)
		
		//Warunki można ze sobą łączyć. Wtedy to muszą one ogólnie spełnić wymagania wartości true by zadziałać.
		
		//AND - jest to odpowiednik mnożenia binarnego (działanie bramki AND). Wszystkie połączone nim warunki muszą dać
		//wartość true
		// np.  (5==5) AND (!false) AND (5!=6)
		//w języku Java zamiast słowa AND stosuje się && (dwa razy znack and)
		//OR - jesto to odpowiednik sumy boinarnej (działąnie braki OR). Tylko jeden z warunków musi zostać spełniony
		//by dać wartość true
		// np. (5==6) OR (!false) OR (5!=5)
		//w języku Java zamiast słowa OR stosuję się || (dwa razy znak kreski pionowej)
		
		//if(haslo.length() > 7) {
		if(haslo.length() >= silaHasla) {
			System.out.println("Hasło spełnia warunki długości hasła.");
			sila = sila + sumaSily/czesciHasla;
		}		
		else if(haslo.length() <= 8 && haslo.length() > 4) {
			System.out.println("Hasło częściowo spełnia warunki długości hasła.");
			System.err.println("Hasło powinno zawierać minimum 8 znaków.");
		}
		else {
			System.err.println("Hasło jest za krótkie! Zawiera " + haslo.length() + " znaków, minimum to 8 znaków.");
		}
		
		/*
		 * For to jedna z podstawowych pętli w programowaniu. Bardzo dobrze pozwala kontrolować działanie kodu w swim wnętrzu.
		 * Generalna zasada - wykonuj kod, który został zapisany w klamrowych nawiasach do chwili, kiedy określony 
		 * warunek jest spełniony. 
		 * 
		 * Zawartość instrukcji sterującej:
		 * - pierwsza instrukcja to przypisanie zmiennej sterującej całym wykonaniem for. Możemy wykorzystać tutaj zmienną
		 * wcześniej zadeklarowaną w kodzie programu bądz, co jest badziej powszechne, zadeklarować tutaj nową zmienną. Zmienna
		 * ta NIE BĘDZIE WIDZIANA POZA CIALEM PĘTLI. 
		 * - druga instrukcja to warunek, który jeżeli przestanie być spełniany to przerwie działanie naszej pętli. Warunki w 
		 * for są identyczne jak warunki dla if
		 * - trzecia instrukcja to instrukcja wywoływana za każdym razem gdy pętla w pełni się wykona (nastąpi jej jedno, pełne
		 * przejście). Najczęściej instrukcją tą jest operacja na zmiennej sterującej (aczkolwiek nie musi to być nic  z nią
		 * związane)
		 * 
		 * np. for (int i = 0; i < 10; i=i+1) -> taka konfiguracja spowoduje, że for wykona się 10 razy (wartość i zmieni się od 
		 * zera do dziewięciu włącznie (10 zakończy działanie)
		 */
		//ciąg znakowy będzie traktowany jak tablica; tablice indeksowane są od ZERA
		
		boolean cyfra, 
				literaM,
				literaD,
				znakS;
		
		String literyD="ĄĆĘŁŃÓŚŹŻ";
		String literyM="ąćęłńóśźż";
		
		cyfra=literaM=znakS=literaD=false;
		for(int i = 0; i < haslo.length(); i=i+1) {
			
			if (cyfra==true && literaM == true && literaD == true && znakS==true)
				break;
			
			//String tmp = haslo.charAt(i)+"";
			//String tmp = String.valueOf(haslo.charAt(i));
			char tmp = haslo.charAt(i);
			
			//rozwiązanie działa ale jest pracochłonne, zajmuje dużą ilość linii i do tego jest ciężkie do modyfikacji
			//gdybyśmy takowej potrzebowali
//			if (haslo.charAt(i) == '0' ||haslo.charAt(i) == '1' ||haslo.charAt(i) == '2' || 
//					haslo.charAt(i) == '3' || haslo.charAt(i) == '4' ||haslo.charAt(i) == '5' ||
//					haslo.charAt(i) == '6'  ||haslo.charAt(i) == '7' || haslo.charAt(i) == '8' ||
//					haslo.charAt(i) == '9' ) {
//				cyfra = true;
//			}
			
			//każdy znak można konwertować jawnie lub niejawnie do wartości całkowitej. Jak wiemy, istnieje coś takie jak
			//tablica ASCII z wartościami liczbowymi znaków; i tutaj to wykorzystamy
			
			//tutaj dla odmiany badamy przedział wartośći musi być pomiędzy wartością 0 oraz wartością 9;
			//gdyby zastosować lub (jako poprzednio) to program sprawdzałby czy coś jest powyżej 0 lub poniżej 9, a tym
			//samym warunek spełniałby dowolny znak 
			//ostatnia zmienna (!cyfra) ma za zadanie chronić kod przed wykonaniem jeżeli jakakolwiek cyfra już została
			//wynaleziona. Zmienna została dołożona z operacją AND w związku z tym też musi być spełniona aby
			//cały warunek zadziałał
			if (tmp >= (int)'0' && tmp <= (int)'9' && !cyfra) {
				cyfra = true;
			}
			
			//tutaj pojawia się potrzeba wyciągnięcia sprawdzania warunku, czy literaM ma wartość true
			if (!literaM)
				if ((tmp >= (int)'a' && tmp <= (int)'z') || literyM.contains(tmp+"")) {
					literaM=true;
				}
			
			if (!literaD)
				if ((tmp >= (int)'A' && tmp <= (int)'Z') || literyD.contains(tmp+"")) 
					literaD=true;
			
			
			//ten if sprawdza czy znakS ma wartość true; jeżeli posiada domyślnie nadaną wartość false to negacja
			//(wykrzyknik) zamieni ją w tym warunku na true (proszę pamiętać, że nie jest to przypisanie nowej wartości
			//do zmiennej!. Jeżeli będzie false to wykona się kod warunku (drugi if) jeżeli zaś będzie true to warunek
			//otrzyma wartość false i podwarunek nie wykona się (zaoszczędzi to czas pracy)
			if (!znakS) 
				//ponieważ badamy zbiory i chcemy WYELIMINOWAĆ występowanie wartości z tych zbiorów,
				//czyli nie chcemy by znaki od A do Z lub od a do z lub 0 do 9 były brane pod uwagę
				//musimy zaprzeczyć ich występowaniu. Dotychcesz, w warunkach powyżej wykazywalismy, że konkretne
				//znaki znajdują się w zbiorze. W związku z tym teraz im ZAPRZECZAMY. Wartość, którą zwraca warunek
				//NEGUJEMY (odwracamy jej wartość). I jeżeli coś nie występuje w zbiorze (false) to dla naszego if 
				//będzie prawdą (bo o to nam chodzi)
				if (!(tmp >= (int)'A' && tmp <= (int)'Z') && 
						!(tmp >= (int)'a' && tmp <= (int)'z') &&
						!(tmp >= (int)'0' && tmp <= (int)'9')) {
					
					//poniższy fragment kodu zamiast przypisywać sztywną wartość (literał) powoduje,
					//że zmienna znakS przyjmie wartość przeciwną do obecnie posiadanej (jeżeli miała wartość false
					//to poprzez zaprzeczenie (wykrzyknik) zmieni wartość na true
					znakS = !znakS;
				}
			
		} 
		
		
		
		if (cyfra) {
			System.out.println("Hasło zawiera przynajmniej jedną cyfrę.");
			sila = sila + sumaSily/czesciHasla;
		}
		else {
			System.err.println("Hasło nie zawiera żadnej cyfry, a powinno przynajmniej jedną!");
		}
		
		if (literaM) {
			System.out.println("Hasło zawiera przynajmniej jedną małą literę.");
			sila = sila + sumaSily/czesciHasla;
		}
		else {
			System.err.println("Hasło nie zawiera żadnej małej litery, a powinno przynajmniej jedną!");
		}
		
		if (literaD) {
			System.out.println("Hasło zawiera przynajmniej jedną dużą literę.");
			sila = sila + sumaSily/czesciHasla;
		}
		else {
			System.err.println("Hasło nie zawiera żadnej dużej litery, a powinno przynajmniej jedną!");
		}
		
		if (znakS) {
			System.out.println("Hasło zawiera przynajmniej jeden znak specjalny.");
			sila = sila + sumaSily/czesciHasla;
		}
		else {
			System.err.println("Hasło nie zawiera żadnego znaku specjalnego, a powinno przynajmniej jeden!");
		}
		
		System.out.print("[");
		for (int i=0; i < sumaSily; i=i+2) {
			if (sila > 0) {
				System.out.print("=");
				sila=sila-2;
			}
			else
				System.out.print(" ");
		}
		
		System.out.println("]");
		System.out.println("Program kończy działanie, do następnego.");
	}

}
