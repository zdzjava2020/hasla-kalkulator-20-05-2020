import java.lang.Math;
import java.util.Scanner;

public class Kalkulator {
	
	public static int pobierz() {
		@SuppressWarnings("resource")
		Scanner wejscie = new Scanner(System.in);
		try {
			return wejscie.nextInt();
		}
		catch (Exception ex) {
			wejscie.next();
		}
		return 0;
	}
	
	public static String pobierzCiag() {
		@SuppressWarnings("resource")
		Scanner wejscie = new Scanner(System.in);
		return  wejscie.nextLine();
	}
	
	public static int menu() {
		final String opcje="\n\nWybierz jedną z opcji:\n1.Dodawanie\n2.Odejmowanie\n3.Mnożenie\n"+
				"4.Dzielenie\n5.Reszta z dzielenia\n6.Potęgowanie\n7.Pierwiastkowanie\nDOWOLNY CIĄG ZNAKÓW - KONIEC PROGRAMU\n"+
							"Twój wybór: ";
		System.out.print(opcje);
		int zwrot=pobierz();
		switch(zwrot) {
			case 1: System.out.print("Podaj wartości do zsumowania: "); break;
			case 2: System.out.print("Podaj wartości do odejmowania: "); break;
			case 3: System.out.print("Podaj wartości do mnożenia: "); break;
			case 4: System.out.print("Podaj wartości do dzielenia: "); break;
			case 5: System.out.print("Podaj wartości do sprawdzenia reszty z dzielenia (dwie): "); break;
			case 6: System.out.print("Podaj wartości podstawy oraz potęgi: "); break;
			case 7: System.out.print("Podaj wartości pierwiastka oraz jego stopnia: ");
					//kolejne linie kodu
					break; 
			default: return 0;
		}
		return zwrot;
	}
	
	public static void wykonajDzialanie(int wybor) {
		String dzialanie =pobierzCiag();
		double a,b;
		a=b=0;
		if (wybor==1) {
			String tmp[] = dzialanie.split(" ");
			String liczby="";
			for(int krok=0;krok<tmp.length;krok++) {
				a=a+Double.valueOf(tmp[krok]);
				liczby=liczby + tmp[krok] + " + ";
				//rzowiązanie działa ale niekoniecznie jest efektywne (ciągły if)
//				if (krok!=tmp.length-1)
//				 liczby=liczby+" + ";
			}
			liczby=liczby.substring(0, liczby.length()-3);
			System.out.println("\n\n" + liczby + " = " + a);
		}
		else if (wybor==2) {
			
		}
		else if (wybor==3) {
					
				}
		else if (wybor==4) {
			
		}
		else if (wybor==5) {
			String tmp[] = dzialanie.split(" ");
			a = Double.valueOf(tmp[0]);
			b = Double.valueOf(tmp[1]);
			System.out.println("\n\nModulo " + a + " % " + b + " = " + a%b);
		}
		else if (wybor==6) {
			String tmp[] = dzialanie.split(" ");
			a = Double.valueOf(tmp[0]);
			b = Double.valueOf(tmp[1]);
			System.out.println("\n\nPotęga z " + a + " stopnia " + b + " wynosi " + Math.pow(a,b));
		}
		else if (wybor==7) {
			String tmp[] = dzialanie.split(" ");
			a = Double.valueOf(tmp[0]);
			b = Double.valueOf(tmp[1]);
			if (a < 0.)
				System.out.println("\n\nNie można obliczyć pierwiastka z wartości ujemnej (błąd!)");
			else if (b != 0.) 
				System.out.println("\n\nPierwiastek z " + a + " stopnia " + b + " wynosi " + Math.pow(a,1.d/b));
			else 
				System.out.println("\n\nPierwiastek ze stopnia 0 wynosi nieskończoność (błąd!)");
		}			
	}
	
	
	
	public static void main(String[] args) {
		final String powitanie="Witaj w programie kalkulator+.\n\nProgram pozwala na.....(blablabla)";
		System.out.print(powitanie);
		
		
		while(true) {
			int wybor = menu();
			if (wybor==0)
				break;
			wykonajDzialanie(wybor);
		}
		
		
		System.out.println("\n\nProgram kończy działanie. Do widzenia!");
	}
}
